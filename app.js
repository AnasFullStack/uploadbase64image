angular.module('myApp', ['naif.base64'])
    .controller('ctrl', function($scope, $http, $window, $rootScope){
        $scope.loadEndHandler = function (e, reader, file, rawFiles, fileObjects, fileObj) {
            //console.log(e);
            //console.log(reader);
            //console.log(file);
            //console.log(rawFiles);
            //console.log(fileObjects);
            //console.log(fileObj.base64);
            var imgBase64 = 'data:image/png;base64,'+fileObj.base64;
            $http.post('server.php', imgBase64)
                .success(function(res){
                    if (res!='Unable to save the file.'){
                        $scope.url=res;
                    }
                    console.log(res);
                });
        };
        $scope.onChange = function (e, fileList) {
            //console.log(e);
            //console.log(fileList);
        };
    });