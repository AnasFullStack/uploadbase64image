# Upload Image as base64 to php server

Using angularJs, bootstrap, php to convert any image to base64 string then upload it to a php server then storing it and giving you back the URL of the stored Image.

### Version
1.0.0

### Tech

Dillinger uses a number of open source projects to work properly:

* [AngularJS] - HTML enhanced for web apps!
* [Twitter Bootstrap] - great UI boilerplate for modern web apps
* [php] 

### Installation
You should have a local apache server install

```sh
$ cd /path/to/your/apache/server
$ git clone https://AnasFullStack@bitbucket.org/AnasFullStack/uploadbase64image.git
$ cd uploadbase64image
$ bower install
Go to http://localhost/uploadbase64image
```


License
----

MIT